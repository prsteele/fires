package fires

import (
	"bitbucket.org/patrickrsteele/go.graph"
	"fmt"
)

type Forest graph.UndirectedGraph

func NewForest(locations []Location, connections []Connection) Forest {

	nodes := make([]graph.Node, len(locations))
	for i, v := range locations {
		nodes[i] = (graph.Node)(v)
	}

	edges := make([]graph.Edge, len(connections))
	for i, v := range connections {
		edges[i] = (graph.Edge)(v)
	}

	return graph.NewUndirectedGraph(nodes, edges)
}

// Represents a portion of a forest. Implements the Node interface.
type Location struct {
	// The latitude of this location
	Lat float64

	// The longitude of this location
	Lon float64

	// The area of this location
	Area float64

	// The probability of this location being the source of a fire
	Prob float64
}

func (loc Location) Equals(other graph.Node) bool {
	switch n := other.(type) {
	case Location:
		return loc == n
	case *Location:
		return loc == *n
	}
	return false
}

func (loc Location) String() string {
	return fmt.Sprintf("{(%.3f, %.3f), A: %.3f, P: %.3f}",
		loc.Lat, loc.Lon, loc.Area, loc.Prob)
}

// Represents a connection between two locations in a forest.
type Connection struct {
	// The two locations
	A, B Location

	// The probability of transmitting a fire
	Prob float64
}

func (c Connection) Tail() graph.Node {
	return c.A
}

func (c Connection) Head() graph.Node {
	return c.B
}

func Locations(f Forest) []Location {
	nodes := f.Nodes()
	locations := make([]Location, len(nodes))
	for i, node := range nodes {
		if loc, ok := node.(Location); !ok {
			panic(fmt.Sprintf("Expected Location, got %v", node))
		} else {
			locations[i] = loc
		}
	}
	return locations
}

func Connections(f Forest) []Connection {
	edges := f.Edges()
	connections := make([]Connection, len(edges))
	for i, edge := range edges {
		if con, ok := edge.(Connection); !ok {
			panic(fmt.Sprintf("Expected Connection, got %v", edge))
		} else {
			connections[i] = con
		}
	}
	return connections
}
