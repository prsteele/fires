package fires

import (
	"bitbucket.org/patrickrsteele/go.graph"
	"fmt"
	"github.com/patrickrsteele/gosim"
	"math/rand"
)

// Returns a new gosim.Trial function, which will be used to produce a
// Monte Carlo estimate of the objective value associated with the
// given graph.
func NewObjectiveTrial(rnd *rand.Rand, forest Forest) gosim.Trial {
	// Extract all locations and connections
	nodes := forest.Nodes()
	locations := Locations(forest)
	connections := Connections(forest)

	// Return a trial function
	return func() (float64, []float64) {
		// Assemble the new graph. Include all connections that are found
		// to transmit fire.
		new_connections := make([]graph.Edge, 0)
		for _, con := range connections {
			if rnd.Float64() <= con.Prob {
				new_connections = append(new_connections, con)
			}
		}

		// Pick a Location for the fire to break out
		var source Location
		found := false
		p := 0.0
		x := rnd.Float64()
		for _, loc := range locations {
			p += loc.Prob
			if p >= x {
				source = loc
				found = true
				break
			}
		}
		if found == false {
			source = locations[len(locations)-1]
		}

		// Create a new graph using the connections selected as
		// transmitting, and return the fitness of the graph
		f := Forest(graph.NewUndirectedGraph(nodes, new_connections))
		return Fitness(f, source), nil
	}
}

// Computes the objective function for a specific forest, given that a
// specific node is the source of a fire
func Fitness(forest Forest, source Location) float64 {
	components := graph.ConnectedComponents(forest)

	for _, component := range components {
		component_value := 0.0
		source_found := false
		for _, node := range component {
			loc, ok := node.(Location)
			if !ok {
				panic(fmt.Sprintf("Expected a Location, not %v", node))
			}

			component_value += loc.Area
			if loc == source {
				source_found = true
			}
		}

		if source_found {

			return component_value
		}
	}

	panic(fmt.Sprintf("Did not find the source node %s in the forest", source))
}
